<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DogBreedController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Dog list
Route::get('/breeds/{page}', [DogBreedController::class, 'breed_index'])->name('breeds');

//Sub breeds
Route::get('/sub_breeds/{breed}', [DogBreedController::class, 'sub_breed_index'])->name('sub_breeds');

//fallback
Route::fallback(function () { return redirect('/breeds/1'); });
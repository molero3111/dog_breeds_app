<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dog Breeds</title>

        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}"/> 
        <script src="https://kit.fontawesome.com/560f0fccc1.js" crossorigin="anonymous"></script>

    </head>
    <body>
        <!--Nav bar-->
        <nav class="navbar navbar-expand-lg dog-text element_color">
            <div class="container-fluid">
                <button class="navbar-toggler border border-light border-3 rounded rounded-3"
                type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" 
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars dog_text"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    <a class="nav-link a_animation" aria-current="page" href="/breeds/1">Dog Breeds</a>
                    </li>
                </ul>
                <span class="navbar-text ">
                    <i class="fas fa-paw fs-1 paw_icon "></i>
                </span>
                </div>
            </div>
            </nav>
        <!--Nav bar end-->
        <div id="content_display" class="container-fluid">
            @yield('content')
        </div>

    </body>
    <footer class="text-center element_color mt-4">
        <h2 class=" pt-3 pb-3">Powered by dog.ceo API!</h2>
        <div class="row fs-5 pb-3">
            <div class="col-md-6">
                The internet's biggest collection of open source dog pictures.
            </div>
            <div class="col-md-6">A business and lifestyle magazine for the modern dog.
            Issue 1 out now. Buy now from Side Orders Publishing. Ships worldwide.</div>
        </div>
        <p class=" fs-3 pb-3">© 2021 Copyright</p>
    </footer>
</html>
@extends('layout')

@section('content')
    @if (count($sub_breeds)  < 1) 

    <h1 class="text-center mt-3 extra_margin">No sub breeds found for the <span class="text-capitalize">{{ $breed }}</span> breed.
    Please click <a class="normal_link" href="/breeds/1">here</a> to go back to home page.</h1>

    @else
        <h1 class="text-center mt-3 mb-3">Sub breeds of <span class="text-capitalize">{{ $breed }}</span></h1>
        <div class="row" id="breeds_cards">
            @foreach ($sub_breeds as $breed)
            <div class="col-md-4">
                <div class="card element_color dog_text mb-3">
                    <img src="{{ $breed['image'] }}" 
                    class="card-img-top card_display_image" alt="{{ $breed['name'] }}">
                    <div class="card-body text-center">
                        <h5 class="card-title text-center capitalize_word">{{ $breed['name'] }}</h5>
                    </div>
                </div>
            </div>
            @endforeach 
        </div>
    @endif
@endsection 
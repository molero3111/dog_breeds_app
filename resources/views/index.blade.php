@extends('layout')

@section('content')
    <h1 class="text-center mt-3 mb-3">Breeds</h1>
    <div class="row" id="breeds_cards">
        @foreach ($breeds as $breed)
        <div class="col-md-4">
            <div class="card element_color dog_text mb-3">
                <img src="{{ $breed['image'] }}" 
                class="card-img-top card_display_image" alt="{{ $breed['name'] }}">
                <div class="card-body text-center">
                    <h5 class="card-title text-center capitalize_word">{{ $breed['name'] }}</h5>
                    <div class="row">
                        <a href="/sub_breeds/{{ $breed['name'] }}" 
                        class="btn btn-dark w-50 mx-auto" target="_blank">Sub breeds</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach 
    </div>
    <!--pagination-->
        <nav class="mt-4 mx-auto">
            <ul class="pagination justify-content-center ">
                @for($i=1; $i<=10; $i++)
                <li class="page-item"><a class="page-link page-border dog_text element_color" href="/breeds/{{$i}}">{{$i}}</a></li>
                @endfor
            </ul>
        </nav>
@endsection  
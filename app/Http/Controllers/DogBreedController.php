<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;
use App\Http\Controllers\Tools;

class DogBreedController extends Controller
{
    //shows all dog breeds
    public function breed_index($page){

        $response = Http::get('https://dog.ceo/api/breeds/list/all');
        $breeds = array_keys($response->json()['message']);
        sort($breeds);
        if($page < 1 || $page > ceil(count($breeds)/10)){$page=1;}

        $page_end = $page*10;
        $page_start = $page_end-10;
        if($page_end > (count($breeds))){
            $page_end=count($breeds);
        }
        
        $breeds = array_slice($breeds, $page_start, 10);
        
        return view('index', ['breeds' =>  Tools::getBreedsWithImages(
        $breeds, 'https://dog.ceo/api/breed/','/images/random')]);

       //return view('index', ['breeds' => count($breeds) ]);        

    }//shows all dog breeds

    public function sub_breed_index($breed){
        $response = Http::get('https://dog.ceo/api/breed/'.$breed.'/list');

        if($response->json()['status']=='error'){
            return view('sub_breeds', [
                'sub_breeds' => []
            ]);
        }

        return view('sub_breeds', ['breed'=>$breed, 'sub_breeds' =>  Tools::getBreedsWithImages(
        $response->json()['message'], 
        'https://dog.ceo/api/breed/'.$breed.'/','/images/random')]);

    }//shows all dog breeds
}

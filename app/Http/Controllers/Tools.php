<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;

class Tools extends Model
{
    public static function getBreedsWithImages($breeds, $urlPart1, $urlPart2){

        $breeds_images = [];
        sort($breeds);
        foreach ($breeds as $breed ) {

            $image_response = Http::retry(3, 8000, function ($exception) {
                return $exception instanceof ConnectionException;
            })->get($urlPart1 . $breed . $urlPart2);
            array_push($breeds_images, ['name'=>$breed,
                'image'=>$image_response->json()['message']]);

        }//foreach
        return $breeds_images;
    }
}
?>